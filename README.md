![colflex logo](https://gitlab.com/supremebot/sppb-colflex/-/wikis/colflex/colflex_logo.png)


# Colflex
##### Colflex improves the responsive capability of SP pagebuilder for Joomla, with flexbox columns.  
With colflex items will automatically wrap to next row if the column width is exceeded.  
Colflex is just a single sass script you easily can add to your template. To make it work, all you have to do is to 
apply the CSS class `colflex` to a column, now you can adjust the addon's width with the "Custom Addon Width" 
under advanced tab.  
That is all it takes to make a self-collapsing responsive columns but of course you can do much more, watch the demo video.  
  
![wrapping addons](https://gitlab.com/supremebot/sppb-colflex/-/wikis/colflex/colflex_wrap.png)

![Demo video "no audio"](https://gitlab.com/supremebot/sppb-colflex/-/wikis/colflex/colflex-demo.mp4)

[Download the demo template from the video here.](https://gitlab.com/supremebot/sppb-colflex/snippets/1969260)

[**Read more about Colflex and how to use it on the WIKI**](https://gitlab.com/supremebot/sppb-colflex/-/wikis)

## Features
- _*_ Set default column count. 
- _*_ Override individual items default size.
- _*_ Set gutter between items. 
- _*_ Align items vertically. 
- _*_ Justify items horizontally. 
- _*_ Auto grow items to always fill out a row. 
- _*_ Reverse item order. 
- _*_ Wrap to a row above instead of below.
- _*_ Disable wrapping. 
- Combine colflex columns with SPPB default responsive column system  
- Sections (inner row) inside a colflex column will also warp, can be used to group addons together.
- Colflex section inside a colflex section.

_*_ ***Responsive feature:*** Can be set with a default value and/or overridden per device breakpoint.  
Example: default we want 3 columns inside the colflex column but only 2 on mobile device.  
Column classes will be: `colflex cols-3 cols-mobil-2`
  
*Note: Colflex is only tested with the free Helix Ultimate template, there is no guarantee that it works with 
other templates*


[**Read more about Colflex and how to use it on the WIKI**](https://gitlab.com/supremebot/sppb-colflex/-/wikis)  



## Install  
Before you install, make sure that "Compile SCSS to CSS" is activated in your templates advanced options.  
These settings are under Extensions > Templates > "Template_Name" > Styles > Template Options.


1) Download and install colflex with git, wget or manuel download (FTP).
   
   - With Git run:  
     Go to the templates sass folder `/templates/"template_name"/scss/`.   
     Run `git clone --depth 1 https://gitlab.com/supremebot/sppb-colflex.git sppb-colflex-master`  
   
   - OR from a terminal with wget:  
     Go to the templates sass folder `/templates/"template_name"/scss/`.
     Run `wget -qO- https://gitlab.com/supremebot/sppb-colflex/-/archive/master/sppb-colflex-master.tar.gz | tar xvfz -`

   - OR manuel download and extract, and transfer the folder to your templates sass folder `/templates/"template_name"/scss/`   
   [Download zip](https://gitlab.com/supremebot/sppb-colflex/-/archive/master/sppb-colflex-master.zip)

2)  In the templates SCSS directory create a new file called **custom.scss**, if you don't have one already.  
    In **custom.scss** import colflex by adding this line `@import "/sppb-colflex-master/colflex";`.  
    In **master.scss** import custom by adding this line `@import "custom";`.  
    
    If you have a terminal you can do this with one line:  
    `echo '@import "sppb-colflex-master/colflex";' >> custom.scss; echo '@import "custom";' >> master.scss`
   
    *Note: The reason why we make custom.scss is so that we have a place for custom setting, with this method
     settings will not be effected if we update colflex*  
     
    ***WARNING:*** SP-pagebuilder sometimes overrides master.scss on large updates, so it may be necessary to reimport 
    **custom.scss** into **master.scss** with `@import custom`
   
## Update
If you installed colflex with Git you can go into the sppb-colflex-master directory and run:  
`git fetch --depth 1; git reset --hard origin`

Else you have to repeat installation step 1 to update.
   
--------
  
